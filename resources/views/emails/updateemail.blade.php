@component('mail::message')
# Hello, {{ $user->name }}

Please verify your account using the following link.

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
